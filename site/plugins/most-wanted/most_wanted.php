<?php
/*
Plugin Name:Most Wanted
Plugin URI: 
Description: This plugin will display headshot photos of the persons and their detail information.
Author: Agile Solutions
Version: 1.1
Author URI: http://agilesolutionspk.com
*/
if (!class_exists('Agile_Most_Wanted')){
	class Agile_Most_Wanted{
		function __construct(){
			add_action( 'admin_init', array(&$this, 'admin_init') );
			register_activation_hook( __FILE__, array(&$this, 'install') );
			register_deactivation_hook( __FILE__, array(&$this, 'de_activate') );
			add_action('wp_enqueue_scripts', array(&$this, 'frontend_scripts') );
			add_action('admin_menu', array(&$this, 'addAdminPage') );
			add_action( 'wp_ajax_addtional_info', array(&$this,'aspk_additional_info' ));
			add_action( 'wp_ajax_nopriv_addtional_info',array(&$this, 'aspk_additional_info' ));
			add_action( 'wp_head', array(&$this, 'head' ));
			add_action('init' , array(&$this,'aspk_custom_post_type_fun'));
			add_shortcode( 'fe_images_view' , array(&$this, 'fe_image_fun' ));
			add_shortcode( 'view_profile' , array(&$this, 'view_profile_fun' ));
			add_shortcode( 'agile_most_wanted_images' , array(&$this, 'view_profile_person' ));
			add_action('init', array(&$this, 'fe_init'));
		}
		function aspk_additional_info(){
		
			$post_id = $_POST['post_id'];
			$key = $_POST['addname'];
			$val = $_POST['addvalue'];
			$add_info = get_post_meta($post_id,'_most_wanted_add_values',true);
			if(empty($add_info)) $add_info=array();
			$add_info[$key] = $val;
			alog('add_info',$add_info,__FILE__,__LINE__);
			update_post_meta($post_id, '_most_wanted_add_values',$add_info);
			echo "OK";
			exit;
		}
		function primary_img(){
			if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
			$uploadedfile = $_FILES['file'];
			$upload_overrides = array( 'test_form' => false );
			$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
				if ( $movefile ) {
					echo "File is valid, and was successfully uploaded.\n";
					var_dump( $movefile);
				} else {
					echo "Possible file upload attack!\n";
				}
		}
		function view_profile_person($atts){
			$post_id = $atts['id'];
			//$wan_img = get_post_meta($post_id,'_aspk_most_wanted_img');
			$wan_img = $this->select_image($post_id);
		
			$wan_info = get_post_meta($post_id,'_most_wanted');
			$wan_info_tattoos = get_post_meta($post_id,'_most_wanted_tattoos', true);
			$user_id = get_current_user_id();
			$user = new WP_User( $user_id );
			$role = $user->roles[0];
			$ad_url = admin_url();
			$url = $ad_url.'admin.php?page=most_wanted&act=edit&postid='.$post_id;
			$approve_url = $ad_url.'admin.php?page=agile_most_wanted_publish&postid='.$post_id;
			$del_url = $ad_url.'admin.php?page=most_wanted&act=del&postid='.$post_id;
			?>
			<div class = "tw-bs container">
				<div class = "row">
					<div class = "col-md-12">
						<p style = "color:#006699;border-bottom:1px solid grey;"><?php echo $wan_info[0]['first_name']." ".$wan_info[0]['last_name'];?></p>
					</div>
				</div>
				<div class = "row">
					<?php if($wan_img[0]->url){ ?>
						<div class = "col-md-4">
							<img src = "<?php echo  $wan_img[0]->url;?>" style = "height:12em; width:12em;" />
						</div>
					<?php } ?>
					<div class = "col-md-8">
						<div class = "row" style = "color:#006699;"><div class = "col-md-12">Physical Description</div></div>
						<div class = "row">
							<div class = "col-md-12">
								DOB:<?php if(!empty($wan_info[0]['dob'])){ 
									echo $wan_info[0]['dob'];
								}?>
							</div>
						</div>
						<div class = "row">
							<div class = "col-md-12">
							RACE:<?php if(!empty($wan_info[0]['race'])){
									echo $wan_info[0]['race'];
								}?>
							</div>
						</div>
						<div class = "row">
							<div class = "col-md-12">
									GENDER:<?php if(!empty($wan_info[0]['gender'])){
										echo $wan_info[0]['gender'];
									}
								?>
							</div>
						</div>
						<div class = "row">
							<div class = "col-md-12">
								HEIGHT:<?php if(!empty($wan_info[0]['height'])){
									echo $wan_info[0]['height'];
								}
								?>
							</div>
						</div>
						<div class = "row">
							<div class = "col-md-12">
								WEIGHT:<?php if(!empty($wan_info[0]['weight'])){
									echo $wan_info[0]['weight'];
								}?>
							</div>
						</div>
						<div class = "row">
							<div class = "col-md-12">
								EYE COLOR:<?php if(!empty($wan_info[0]['eye_color'])){		echo $wan_info[0]['eye_color'];
							}?>
							</div>
						</div>
						<div class = "row">
							<div class = "col-md-12">
								HAIR COLOR:<?php if(!empty($wan_info[0]['hair_color'])){
									echo $wan_info[0]['hair_color'];
								}?>
							</div>
						</div>
					</div>
				</div>
					<div class = "row">
						<div class = "col-md-12" style = "color:#006699;margin-left:14em;"><?php if(!empty($wan_info_tattoos['scars'])){echo "SCARS";}if(!empty($wan_info_tattoos['marks'])){echo " MARKS";}if(!empty($wan_info_tattoos['tattoos'])){echo "TATTOOS";}?>
						</div>
					</div>
					<div class = "row">
						<div class = "col-md-12" style = "margin-left:14em;"><?php echo $wan_info_tattoos['scars'].' '.$wan_info_tattoos['marks'].' '.$wan_info_tattoos['tattoos'];?></div>
					</div>
					<div class = "row">
						<div class = "col-md-12" style = "color:#006699;">INCARCERATION DETAILS</div>
					</div>
					<div class = "row">
						<div class = "col-md-12">
							MAJOR OFFENCE:<?php if(!empty($wan_info[0]['major_offense'])){echo $wan_info[0]['major_offense'];}?>
						</div>
					</div>
					<div class = "row">
						<div class = "col-md-12">
							MOST RECENT INSTITUTION:<?php if(!empty( $wan_info[0]['most_recent_institute'])){echo $wan_info[0]['most_recent_institute'];}?>
						</div>
					</div>
					<div class = "row">
						<div class = "col-md-12">
							MAX POSSIBLE RELEASE DATE:<?php if(!empty($wan_info[0]['max_possible_release_date'])){echo $wan_info[0]['max_possible_release_date'];}?>
						</div>
					</div>
					<div class = "row">
						<div class = "col-md-12">
							ACTUAL RELEASE DATE:<?php if(!empty($wan_info[0]['actual_release_date'])){ echo $wan_info[0]['actual_release_date'];}?>
						</div>
					</div>
					<div class = "row">
						<div class = "col-md-12">
							CURRENT STATUS:<?php if(!empty($wan_info[0]['current_status'])){echo $wan_info[0]['current_status'];}?>
						</div>
					</div>
					<div class = "row">
						<div class = "col-md-12" style = "color:#006699;">KNOWN ALIASES</div>
					</div>
					<div class = "row">
						<div class = "col-md-12">
							<?php if(!empty($wan_info[0]['known_aliases'])){echo $wan_info[0]['known_aliases'];}?>
						</div>
					</div>
					<script>
						function del_js(){
							var con = confirm('Are You Sure You Want To Delete');
							if(con == true){
								return true;
							}else{
								return false;
							}
						}
					</script>
					<?php if($role == 'administrator'){ ?>
					<div class = "row">
						<div class = "col-md-4"><a href = "<?php echo $url;?>" ><button>Edit</button></a></div>
						<div class = "col-md-4"><a href = "<?php echo $approve_url; ?>" ><button>Approve</button></a></div>
						<div class = "col-md-4"><a href = "<?php echo $del_url; ?>" onclick = "return del_js()"><button>Delete</button></a></div>
					</div>
					<?php } ?>
			</div>
			<?php
			
		}
		function install(){
			global $wpdb;
			
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			
			$sql="CREATE TABLE `".$wpdb->prefix."aspk_img_urls_paths` (
				  `id` int(255) NOT NULL AUTO_INCREMENT,
				  `uuid` varchar(255) DEFAULT NULL,
				  `pid` int(255) DEFAULT NULL,
				  `url` varchar(500) DEFAULT NULL,
				  `path` varchar(500) DEFAULT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
				";
			dbDelta($sql);
			
			
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			
			$sql2="CREATE TABLE `".$wpdb->prefix."aspk_primary_images` (
				  `id` int(255) NOT NULL AUTO_INCREMENT,
				  `uuid` varchar(255) DEFAULT NULL,
				  `pid` int(255) DEFAULT NULL,
				  `url` varchar(500) DEFAULT NULL,
				  `path` varchar(500) DEFAULT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
				";
			dbDelta($sql2);
		}
		function select_image($postid){
			global $wpdb;
			
			$sql="SELECT * FROM {$wpdb->prefix}aspk_img_urls_paths WHERE pid = '{$postid}'";
			$rs = $wpdb->get_results($sql);
			return $rs;
		}
		function select_image_primary(){
			global $wpdb;
			
			$sql="SELECT * FROM {$wpdb->prefix}aspk_primary_images ORDER BY pid DESC";
			$rs = $wpdb->get_results($sql);
			return $rs;
		}
		function select_image_primary_for_profile($post_id){
			global $wpdb;
			
			$sql="SELECT url FROM {$wpdb->prefix}aspk_primary_images WHERE pid = '{$post_id}'";
			$rs = $wpdb->get_results($sql);
			return $rs;
		}
		function de_activate(){}
		function frontend_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_style('bootstrap_iii_cs', plugins_url('css/tw-bs.3.1.1.css', __FILE__) );
			wp_enqueue_style( 'aspk_front_post_style_admin', plugins_url('css/fineuploader.css', __FILE__) );
			wp_enqueue_script('bootstrap_iii_js', plugins_url('js/bootstrap.min.js', __FILE__) );
			wp_enqueue_script('agile-images-uploader-admin',plugins_url('js/fineuploader.min.js', __FILE__));
		}
		function head(){
			echo '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
			echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
		}
		function fe_init(){}
		function fe_image_fun(){
			//GETTING TEXTAREA CONTENTS USING GET OPTION
			$v = get_option('textarea');
			$banner = get_option( "_aspk_banner_img",0);
			alog('ba',$banner,__FILE__,__LINE__);
			if(isset($_GET['st'])){
				$st = $_GET['st'];
			}else{
				$st = 1;
			}
			$count_posts = wp_count_posts( 'most_wanted' )->publish;
			$argument = array(
				'post_type' => 'most_wanted',
				'post_status' => 'publish',
				'posts_per_page' => '12',
				'paged' => $st
			);
			$user_posts = get_posts($argument);
			?>
				<div class = "tw-bs container">
					<?php
						$pagetitle = get_page_by_title('Profile');
						$permalink = get_permalink($pagetitle->ID);
						$img_url = $this->select_image_primary();
						foreach($img_url as $images){
							$postid = $images->pid;	
							$info = get_post_meta($postid,'_most_wanted',true);
							
					?>
						<div class = "row">
							<div class = "col-md-4" style="margin-top:1em;clear:left;">&nbsp;
								<a href = "<?php echo $permalink.'?agilepostid='.$postid;?>">
									<img src = "<?php echo  $images->url;?>"style = "height:10em; width:10em;"/>
								</a>
							</div>
							<div class = "col-md-8" style="">&nbsp;
							<?php if(!empty($info['first_name'])){ ?>
								<div class = "row">
								<div class = "col-md-4">Name:</div>
								<div class = "col-md-8"><?php echo $info['first_name'];?></div>
								</div>
							<?php } ?>
							<?php if(!empty($info['dob'])){ ?>
								<div class = "row">
								<div class = "col-md-4">DOB:</div>
									<div class = "col-md-8"><?php echo $info['dob'];?></div>
								</div>
							<?php } ?>
							<?php if(!empty($info['gender'])){ ?>
								<div class = "row">
								<div class = "col-md-4">Gender:</div>
									<div class = "col-md-8"><?php echo $info['gender'];?></div>
								</div>
							<?php } ?>
							<?php if(!empty($info['height'])){ ?>
								<div class = "row">
									<div class = "col-md-4">Height:</div>
										<div class = "col-md-8"><?php echo $info['height'];?></div>
								</div>
							<?php } ?>
							<?php if(!empty($info['weight'])){ ?>
								<div class = "row">
									<div class = "col-md-4">Weight:</div>
										<div class = "col-md-8"><?php echo $info['weight'];?></div>
								</div>
							<?php } ?>
							<?php if(!empty($info['eye_color'])){ ?>
								<div class = "row">
									<div class = "col-md-4">Eye Color:</div>
										<div class = "col-md-8"><?php echo $info['eye_color'];?></div>
								</div>
							<?php } ?>
							</div>
							
						</div>
					
						<?php } 
						if($banner){
						?>
						<div class = "row">
							<div class = "col-md-12" style="clear:left;margin-top:1em;">&nbsp;
								<img src="<?php echo $banner['url']; ?>">
							</div>
						</div>
						<?php
						}
						if( $st > 1){
							$pst = $st - 1;
						}
						$st = $st + 1;
						if($count_posts > 3){
							$link = get_permalink();
							?>
							<div class = "row">
							<?php if(!empty($pst)){ ?>
								<div class = "col-md-3"><a href = "<?php echo $link.'?st='.$pst;?>" >Previous</a></div>
							<?php } ?>
								<div class = "col-md-4"><a href = "<?php echo $link.'?st='.$st;?>" >Next</a></div>
							</div>
							<?php
						}
					?>		
				</div>
				<?php
		}
		function view_profile_fun(){
			$wanted_id = $_GET['agilepostid'];
			$wan_img = $this->select_image_primary_for_profile($wanted_id);
			$wan_img_sec = $this->select_image($wanted_id);
			if(!empty($wanted_id)){
				$wan_info = get_post_meta($wanted_id,'_most_wanted');
				$wan_info_tattoos = get_post_meta($wanted_id,'_most_wanted_tattoos', true);
				$user_id = get_current_user_id();
				$user = new WP_User($user_id);
				$role = $user->roles[0];
				$ad_url = admin_url();
				$url = $ad_url.'admin.php?page=most_wanted&act=edit&postid='.$wanted_id;
				$approve_url = $ad_url.'admin.php?page=agile_most_wanted_publish&postid='.$wanted_id;
				$del_url = $ad_url.'admin.php?page=most_wanted&act=del&postid='.$wanted_id;
				?>
					<div class = "tw-bs container">
						<div class = "row">
							<div class = "col-md-12">
								<p style = "color:#006699;border-bottom:1px solid grey;"><?php echo $wan_info[0]['first_name']." ".$wan_info[0]['last_name'];?></p>
							</div>
						</div>
						<div class = "row">
							<div class = "col-md-4">
								<img src = "<?php echo $wan_img[0]->url;?>" style = "height:12em; width:12em;"/>
							</div>
							<div class = "col-md-8">
								<div class = "row" style = "color:#006699;"><div class = "col-md-12">Physical Description</div></div>
								<div class = "row">
									<div class = "col-md-12">
										DOB:<?php if(!empty($wan_info[0]['dob'])){echo $wan_info[0]['dob'];}?>
									</div>
								</div>
								<div class = "row">
									<div class = "col-md-12">
										RACE:<?php if(!empty($wan_info[0]['race'])){echo $wan_info[0]['race'];}?>
									</div>
								</div>
								<div class = "row">
									<div class = "col-md-12">
										GENDER:<?php if(!empty($wan_info[0]['gender'])){echo $wan_info[0]['gender'];}?>
									</div>
								</div>
								<div class = "row">
									<div class = "col-md-12">
										HEIGHT:<?php if(!empty($wan_info[0]['height'])){echo $wan_info[0]['height'];}?>
									</div>
								</div>
								<div class = "row">
									<div class = "col-md-12">
										WEIGHT:<?php if(!empty($wan_info[0]['weight'])){echo $wan_info[0]['weight'];}?>
									</div>
								</div>
								<div class = "row">
									<div class = "col-md-12">
										EYE COLOR:<?php if(!empty($wan_info[0]['eye_color'])){ echo $wan_info[0]['eye_color'];}?>
									</div>
								</div>
								<div class = "row">
									<div class = "col-md-12">
										HAIR COLOR:<?php if(!empty($wan_info[0]['hair_color'])){echo $wan_info[0]['hair_color'];}?>
									</div>
								</div>
							</div>
						</div>
						<div class = "row">
							<div class = "col-md-12" style = "color:#006699;margin-left:14em;"><?php if(!empty($wan_info_tattoos['scars'])){ echo "SCARS";}if(!empty($wan_info_tattoos['marks'])){ echo " MARKS";}if(!empty($wan_info_tattoos['tattoos'])){ echo "  TATTOOS";}?>
							</div>
						</div>
						<div class = "row">
							<div class = "col-md-12" style = "margin-left:14em;"><?php echo $wan_info_tattoos['scars'].' '.$wan_info_tattoos['marks'].' '.$wan_info_tattoos['tattoos'];?></div>
						</div>
						<div class = "row">
							<div class = "col-md-12" style = "color:#006699;">INCARCERATION DETAILS</div>
						</div>
						<?php if($wan_info[0]['major_offense']){ ?>
						<div class = "row">
							<div class = "col-md-12">
								MAJOR OFFENCE:<?php if(!empty($wan_info[0]['major_offense'])){echo $wan_info[0]['major_offense'];}?>
							</div>
						</div>
						<?php } ?>
						<?php if($wan_info[0]['most_recent_institute']){ ?>
						<div class = "row">
							<div class = "col-md-12">
								MOST RECENT INSTITUTION:<?php if(!empty($wan_info[0]['most_recent_institute'])){echo $wan_info[0]['most_recent_institute'];}?>
							</div>
						</div>
						<?php } ?>
						<?php if($wan_info[0]['max_possible_release_date']){ ?>
						<div class = "row">
							<div class = "col-md-12">
								MAX POSSIBLE RELEASE DATE:<?php if(!empty($wan_info[0]['max_possible_release_date'])){echo $wan_info[0]['max_possible_release_date'];}?>
							</div>
						</div>
						<?php } ?>
						<?php if($wan_info[0]['actual_release_date']){ ?>
						<div class = "row">
							<div class = "col-md-12">
								ACTUAL RELEASE DATE:<?php if(!empty($wan_info[0]['actual_release_date'])){echo $wan_info[0]['actual_release_date'];}?>
							</div>
						</div>
						<?php } ?>
							<?php if($wan_info[0]['current_status']){ ?>
						<div class = "row">
							<div class = "col-md-12">
								CURRENT STATUS:<?php if(!empty($wan_info[0]['current_status'])){echo $wan_info[0]['current_status'];}?>
							</div>
						</div>
						<?php } ?>
						<div class = "row">
							<div class = "col-md-12" style = "color:#006699;">KNOWN ALIASES</div>
						</div>
						<div class = "row">
							<div class = "col-md-12">
								<?php if(!empty($wan_info[0]['known_aliases'])){echo $wan_info[0]['known_aliases'];}?>
							</div>
						</div>
						<script>
							function del_js(){
								var con = confirm('Are You Sure You Want To Delete');
								if(con == true){
									return true;
								}else{
									return false;
								}
							}
						</script>
						<?php foreach($wan_img_sec as $image_sec){ 
						alog('$wan_img_sec',$wan_img_sec,__FILE__,__LINE__);
						if($image_sec->url){
					?>
						<div class = "row">
								<div style="margin-bottom: 2em;" class = "col-md-4"><img style="height:10em; width:10em;" src = "<?php echo $image_sec->url;?>"/></div>
						</div>
						<?php 
						}
					} ?>
						<?php if($role == 'administrator'){ ?>
						<div class = "row" style="clear:left;">
							<div class = "col-md-3 btn btn-primary"><a style="text-decoration:none;color:white;" href = "<?php echo $url; ?>" >Edit</a></div>
							<div class="col-md-1"></div>
							<div class = "col-md-3 btn btn-primary"><a style="text-decoration:none;color:white;" href = "<?php echo $approve_url; ?>" >Approve</a></div>
							<div class="col-md-1"></div>
							<div class = "col-md-3 btn btn-primary"><a style="text-decoration:none;color:white;" href = "<?php echo $del_url ;?>"onclick = "return del_js()">Delete</a></div>
						</div>
						<?php } ?>
					</div>
					
				<?php
			}else{
				echo "<h4>Post not found</h4>";
			}
		}
		function admin_init(){
			wp_enqueue_script('jquery');
			wp_enqueue_style('bootstrap_iii_cs', plugins_url('css/tw-bs.3.1.1.css', __FILE__) );
			wp_enqueue_style( 'aspk_front_post_style_admin', plugins_url('css/fineuploader.css', __FILE__) );
			wp_enqueue_script('bootstrap_iii_js', plugins_url('js/bootstrap.min.js', __FILE__) );
			wp_enqueue_script('agile-images-uploader-admin',plugins_url('js/fineuploader.min.js', __FILE__));
		}
		function addAdminPage(){
			add_menu_page('most_wanted', 'Most Wanted', 'edit_posts', 'most_wanted', array(&$this, 'most_wanted_settings'));	
			add_submenu_page('', 'Most Wanted Publish', 'Most Wanted Publish', 'manage_options','agile_most_wanted_publish', array(&$this, 'most_wanted_publish'));
			add_submenu_page('most_wanted','Grid Setting','Grid Setting','manage_options', 'aspk_grid_settings', array(&$this,'most_wanted_grid'));
			add_submenu_page('most_wanted','Pending Approvals','Pending Approvals','manage_options', 'aspk_post_approval_pending', array(&$this,'most_wanted_post_approval'));
		}
		//FUNCTION FOR SHOW PENDING APPROVAL POSTS 
		function most_wanted_post_approval(){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}posts WHERE post_status = 'draft' AND post_type = 'most_wanted'";
			$rs = $wpdb->get_results($sql);
			$pagetitle = get_page_by_title('Profile');
			$permalink = get_permalink($pagetitle->ID);
			?>
				<div class = "tw-bs container">
					<div class = "row">
						
						<h5><div class = "col-md-3">
							Post Title
						</div></h5>
						<h5><div class = "col-md-3">
							Post Date
						</div></h5>
						<h5><div class = "col-md-3">
							Post Status
						</div></h5>
						<div class = "col-md-3">
							&nbsp;
						</div>
					</div>
					<div class = "row">
						<?php foreach($rs as $r){ ?>
							<?php $mwid = $r->ID; 
							$link_send = $permalink.'?agilepostid='.$mwid;
							?>
							<div class = "col-md-3">
								<a href = "<?php echo $link_send;?>"><?php echo $r->post_title;?></a>
							</div>
							<div class = "col-md-3">
								<?php echo $r->post_date;?>
							</div>
							<div class = "col-md-3">
								<?php echo ucfirst($r->post_status);?>
							</div>
							<div class = "col-md-3">
								&nbsp;
							</div>
						<?php } ?>
					</div>
				</div>
			<?php
		}
		function upload_banner_img(){
			if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
			$uploadedfile = $_FILES['banerfile'];
			$upload_overrides = array( 'test_form' => false );
			$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
			if ( $movefile ) {
				$x = array();
				$x['file'] = $movefile['file'];
				$x['url'] = $movefile['url'];
				update_option("_aspk_banner_img",$x);
			}
		}
		function most_wanted_grid(){
			if(isset($_FILES['banerfile'])){
					$this->upload_banner_img();
			}
			if(isset($_POST['update_setting'])){
				$textarea = array();
				$textarea['textarea'] = $_POST['textarea'];
				$textarea['flyer_textarea'] = $_POST['flyer_textarea'];
				update_option('_aspk_mw_textarea',$textarea);
			}
			$setting_content = get_option('_aspk_mw_textarea',array());
			?>
				<div class = "tw-bs container">
					<form action="" method="post" enctype="multipart/form-data">
						<div class = "row">
							<div class = "col-md-12">
								<div class="form-group">
									<h4>Grid editor:</h4>
									<textarea name = "textarea" style = "width:45em;" rows = "8" style="overflow:auto;"><?php echo $setting_content['textarea'];?></textarea>
								</div>
							</div>
						</div>
						<div class = "row">
							<div class = "col-md-12">
								<div class="form-group">
									<h4>Upload Flyer:</h4>
									<div style="width: 233px; height: 42px;
									background: url(<?php echo plugins_url('img/uplodfile.png', __FILE__);?>);overflow: hidden;"class="upload">
										<input style="display: block !important; width: 233px !important; height: 42px !important;opacity: 0 !important;"type="file" name="banerfile" id="banner_file"/>
									</div>
								</div>
							</div>
						</div>
						<div class = "row">
							<div class = "col-md-2">
								<input class="btn btn-default btn-lg btn-block" type = "submit" name = "update_setting" id = "update_setting" value = "Save"/>
							</div>
						</div>
					</form>
				</div>
			<?php
			$textarea = $_POST['textarea'];
			update_option('textarea',$textarea);
		}
		function most_wanted_publish(){
			$post_id = $_GET['postid'];
			$pmeta = get_post_meta($post_id, '_most_wanted', true);
			$p = get_post($post_id, ARRAY_A);
			$p['ID']			=	$post_id;
			$p['post_title']	= 	$pmeta['first_name'].' '.$pmeta['last_name'];
			$p['post_status']	= 'Publish';
			$p['post_type'] 	= 'most_wanted';
			wp_update_post( $p );
			$page = get_page_by_title($pmeta['first_name'].' '.$pmeta['last_name']);
			$page_id = $page->ID;
			$user_id = get_current_user_id();
			$post = array( 
				'ID'			 => $page_id,
				'post_content'   => '[agile_most_wanted_images id="'.$post_id.'"]',
				'post_name'      => $pmeta['first_name'].'_'.$pmeta['last_name'],
				'post_title'     => $pmeta['first_name'].' '.$pmeta['last_name'], 
				'post_status'    => 'Publish', 
				'post_type'      => 'page', 
				'post_author'    => $user_id
			); 
			wp_update_post($post);
			echo "<h3>Post Publish Successfully and Page Created.</h3>";
		}
		function aspk_delete_post(){
			global $wpdb;
			
			$dt = date('Y-m-d h:i:s');
			$sql="Delete FROM {$wpdb->prefix}posts WHERE post_title = 'del-me' AND post_date < '{$dt}'";
			$wpdb->query($sql);
		}
		//FUNCTION FOR CREATING MOST WANTED POST AS PAGE
		function create_most_wanted_page($x, $pid, $role){
			$user_id = get_current_user_id();
			$page = get_page_by_title($x['first_name'].' '.$x['last_name']);
			$page_id = $page->ID;
			if($role == 'administrator'){
				$post = array( 
					'ID'			 => $page_id,
					'post_content'   => '[agile_most_wanted_images id="'.$pid.'"]',
					'post_name'      => $x['first_name'].'_'.$x['last_name'],
					'post_title'     => $x['first_name'].' '.$x['last_name'], 
					'post_status'    => 'publish', 
					'post_type'      => 'page', 
					'post_author'    => $user_id
				); 
			}else{
				$post = array( 
					'post_content'   => '[agile_most_wanted_images id="'.$pid.'"]',
					'post_name'      => $x['first_name'].'_'.$x['last_name'],
					'post_title'     => $x['first_name'].' '.$x['last_name'], 
					'post_status'    => 'draft', 
					'post_type'      => 'page', 
					'post_author'    => $user_id
				); 
			}
			if(!empty($page_id)){
				wp_update_post( $post );
			}else{			
				wp_insert_post($post);
			}
		} //END FUNCTION FOR CREATING MOST WANTED POST AS PAGE
		function insert_post(){
			$my_post = array(
				'post_title'    => 'Del me',
				'post_status'   => 'draft',
				'post_type'		=> 'most_wanted'
			);
			$post_id = wp_insert_post($my_post);
			return $post_id;
		}
		function aspk_update_post($pmeta,$pid,$s,$role){
			if($role == 'administrator'){
				$p = get_post($pid, ARRAY_A);
				$p['ID']			=	$pid;
				$p['post_title']	= 	$pmeta['first_name'].' '.$pmeta['last_name'];
				$p['post_status']	= 'publish';
				$p['post_type'] 	= 'most_wanted';
			}else{
				$p = get_post($pid, ARRAY_A);
				$p['ID']			=	$pid;
				$p['post_title']	= 	$pmeta['first_name'].' '.$pmeta['last_name'];
				$p['post_status']	= 'draft';
				$p['post_type'] 	= 'most_wanted';
			}
			wp_update_post($p);
			update_post_meta($pid, '_most_wanted',$pmeta);
			update_post_meta($pid, '_most_wanted_tattoos',$s);
			if($role != 'administrator'){
				$pagetitle = get_page_by_title('Profile');
				$permalink = get_permalink($pagetitle->ID);
				$link_send = $permalink.'?agilepostid='.$pid;
				$to = $this->get_admin_emails();
				wp_mail( $to, 'Link For Approve Post', $link_send ); 
			}
			$this->aspk_delete_post();	
		}
		function get_admin_emails(){
			$args = array(
			'blog_id'      => $GLOBALS['blog_id'],
			'role'         => 'administrator',
			);
			$args2 = array(
			'blog_id'      => $GLOBALS['blog_id'],
			'role'         => 'editor',
			);
			$user1 = get_users($args);
			foreach($user1 as $ad_mail){
				$admin_email = $ad_mail->user_email;
			}
			$user2 = get_users($args2);
			foreach($user2 as $ed_mail){
				$editor_email = $ed_mail->user_email;
			}
			$team = 'team1.aspk@gmail.com';
			return $mail = array($admin_email,$editor_email,$team );
			//returns array of emails
		}
		function del_post($pid){
			$wan_info = get_post_meta($pid,'_most_wanted');
			$page = get_page_by_title($wan_info[0]['first_name']." ".$wan_info[0]['last_name']);
			$page_id = $page->ID;
			wp_delete_post($page_id,true);
			delete_post_meta($pid,'_most_wanted');
			delete_post_meta($pid,'_aspk_most_wanted_img');
			wp_delete_post($pid,true);
		}
		function delete_mostwanted_post($del_id){
			global $wpdb;
			
			//del page
			$mwpost = get_post($del_id);
			$p = get_page_by_title( $mwpost->post_title);
			wp_delete_post($p->ID,true);
			//del meta
			delete_post_meta($del_id, '_most_wanted');
			$sql = "delete from {$wpdb->prefix}aspk_img_urls_paths where pid='{$del_id}'";
			$wpdb->query($sql); 
			//del post
			wp_delete_post( $del_id, true );
		}
		function most_wanted_settings(){
			//check for del
			if(isset($_GET['act'])){
				if($_GET['act']=='del'){
					$del_id = $_GET['postid'];
					$this->delete_mostwanted_post($del_id);
				}
			}
			//check for del ends
			$user_id = get_current_user_id();
			$user = new WP_User( $user_id );
			$role = $user->roles[0];
			if(isset($_POST['submit'])){
				$x = array();
				$x['first_name'] = $_POST['fname'];
				$x['last_name'] = $_POST['lname'];
				$x['dob'] = $_POST['dob'];
				$x['eye_color'] = $_POST['eye_color'];
				$x['gender'] = $_POST['gender'];
				$x['hair_color'] = $_POST['hair_color'];
				$x['height'] = $_POST['height'];
				$x['weight'] = $_POST['weight'];
				$x['race'] = $_POST['race'];
				$x['major_offense'] = $_POST['major_offense'];
				$x['most_recent_institute'] = $_POST['most_recent_institute'];
				$x['max_possible_release_date'] = $_POST['max_possible_release_date'];
				$x['actual_release_date'] = $_POST['actual_release_date'];
				$x['current_status'] = $_POST['current_status'];
				$x['known_aliases'] = $_POST['known_aliases'];
				$post_id = $_POST['pid'];
				$s = array();
				$s['scars'] = $_POST['scars'];
				$s['tattoos'] = $_POST['tattoos'];
				$s['marks'] = $_POST['marks'];
				$this->aspk_update_post($x,$post_id,$s,$role);
				$this->create_most_wanted_page($x,$post_id,$role);
			}else{
				if(isset($_GET['act'])){
					$del_pid = $_GET['postid'];
					$action = $_GET['act'];
					if($action == 'del'){
						$this->del_post($del_pid);
					}elseif($action == 'edit'){
						$x = get_post_meta($del_pid, '_most_wanted', true);
						$s = get_post_meta($del_pid, '_most_wanted_tattoos', true);
					}
				}else{
					$pid = $this->insert_post();
				}
			}
		?>
	<div class = "tw-bs container">	
		<script>
			jQuery(document).ready(function(){
				jQuery('#thumbnail-fine-uploader').fineUploader({
				template: "simple-previews-template",
					thumbnails:{
						placeholders: {
						/* waitingPath: "assets/waiting-generic.png",
						notAvailablePath: "assets/not_available-generic.png" */
					  }
				  },
				deleteFile: {
					enabled: true,
					method: "POST",
					endpoint: '<?php echo home_url("wp-content/plugins/most-wanted/fineup_server.php?act=del");?>'
				},
				request:{
					endpoint: '<?php echo home_url("wp-content/plugins/most-wanted/fineup_server.php?pid=".$pid."&edit=".$edit_post_id);?>'
				}	
				}).on("complete", function(useless, id, fileName, resp){
					//jQuery('#thumbnail-fine-uploader').append('<img height="113" width="93" src="'+resp.url+'">'+'<a href="#" onClick ="jqdelete(\''+resp.uuid+'\');">Delete</a>');
					//jQuery('.qq-upload-success').hide();
				});
			}); 
		</script>

			<h2>M-W</h2>
					<div class="row" >
					<div class="col-md-4"></div>
					<div class="col-md-4">UPLOAD MULTI IMAGES</div>
					<div class="col-md-4"></div>
					</div>
		<div class="row">
			<div class="col-md-12">
					<div id="thumbnail-fine-uploader"></div>
				<script type="text/template" id="simple-previews-template">
					<div class="qq-uploader-selector qq-uploader">
						<div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
							<span>Drop files here to upload</span>
						</div>
						<div style="width:50em;" class="qq-upload-button-selector qq-upload-button">
							<div>Upload a File</div>
						</div>
						  <span class="qq-drop-processing-selector qq-drop-processing">
							  <span>Processing dropped files...</span>
							  <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
						  </span>
						<ul class="qq-upload-list-selector qq-upload-list">
							<li>
								<div class="qq-progress-bar-container-selector">
									<div class="qq-progress-bar-selector qq-progress-bar"></div>
								</div>
								<span class="qq-upload-spinner-selector qq-upload-spinner"></span>
								<img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
								<span class="qq-edit-filename-icon-selector qq-edit-filename-icon"></span>
								<span class="qq-upload-file-selector qq-upload-file"></span>
								<input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
								<span class="qq-upload-size-selector qq-upload-size"></span>
								<a class="qq-upload-cancel-selector btn-small btn-warning" href="#">Cancel</a>
								<a class="qq-upload-retry-selector btn-small btn-info" href="#">Retry</a>
								<a class="qq-upload-delete-selector btn-small btn-warning" href="#">Delete</a>
								<span class="qq-upload-status-text-selector qq-upload-status-text"></span>
								<a class="view-btn btn-small btn-info hide" target="_blank">View</a>
							</li>
						</ul>
					</div>
				</script>
			</div>		
		</div><!--row-->
		<script>
				jQuery(document).ready(function(){
					jQuery('#thumbnail-fine-uploader-2').fineUploader({
					template: "simple-previews-template",
						thumbnails:{
							placeholders: {
							/* waitingPath: "assets/waiting-generic.png",
							notAvailablePath: "assets/not_available-generic.png" */
						  }
					  },
					deleteFile: {
						enabled: true,
						method: "POST",
						endpoint: '<?php echo home_url("wp-content/plugins/most-wanted/fineup_server.php?act=del&prim=yes");?>'
					},
					multiple: true,
						validation: {
							allowedExtensions: ['png','gif','jpg'],
							sizeLimit: 2097152,
							itemLimit: 1
						},
					request:{
						endpoint: '<?php echo home_url("wp-content/plugins/most-wanted/fineup_server.php?pid=".$pid."&edit=".$edit_post_id."&prim=yes");?>'
					}
						
					}).on("complete", function(useless, id, fileName, resp) {
						//jQuery('#thumbnail-fine-uploader').append('<img height="113" width="93" src="'+resp.url+'">'+'<a href="#" onClick ="jqdelete(\''+resp.uuid+'\');">Delete</a>');
						//jQuery('.qq-upload-success').hide();
					  });
				}); 
			</script>
			<div class="row" >
				<div class="col-md-4"></div>
				<div class="col-md-4">UPLOAD PRIMARY IMAGE</div>
				<div class="col-md-4"></div>
			</div>
		<div class="row">
			<div class="col-md-12">
					<div id="thumbnail-fine-uploader-2"></div>
				<script type="text/template" id="simple-previews-template">
					<div class="qq-uploader-selector qq-uploader">
						<div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
							<span>Drop files here to upload</span>
						</div>
						<div style="width:50em;" class="qq-upload-button-selector qq-upload-button">
							<div>Upload a Primary Image</div>
						</div>
						  <span class="qq-drop-processing-selector qq-drop-processing">
							  <span>Processing dropped files...</span>
							  <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
						  </span>
						<ul class="qq-upload-list-selector qq-upload-list">
							<li>
								<div class="qq-progress-bar-container-selector">
									<div class="qq-progress-bar-selector qq-progress-bar"></div>
								</div>
								<span class="qq-upload-spinner-selector qq-upload-spinner"></span>
								<img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
								<span class="qq-edit-filename-icon-selector qq-edit-filename-icon"></span>
								<span class="qq-upload-file-selector qq-upload-file"></span>
								<input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
								<span class="qq-upload-size-selector qq-upload-size"></span>
								<a class="qq-upload-cancel-selector btn-small btn-warning" href="#">Cancel</a>
								<a class="qq-upload-retry-selector btn-small btn-info" href="#">Retry</a>
								<a class="qq-upload-delete-selector btn-small btn-warning" href="#">Delete</a>
								<span class="qq-upload-status-text-selector qq-upload-status-text"></span>
								<a class="view-btn btn-small btn-info hide" target="_blank">View</a>
							</li>
						</ul>
					</div>
				</script>
			</div>		
		</div><!--row-->
	</div><!--end uplodr container-->
		<div class = "tw-bs container">	
			<form method="post" action="" enctype="multipart/form-data">
			
				<div class="row">
					<div class="col-md-12">&nbsp;</div>
				</div><!--row-->
				
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">FIRST NAME*</div>
					<div class="col-md-4"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<input class="form-control" value = "<?php echo $x['first_name'];?>" type="text" name="fname" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">LAST NAME*</div>
					<div class="col-md-4"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<input class="form-control" type="text"  value = "<?php echo $x['last_name'] ;?>" name="lname" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">DOB*</div>
					<div class="col-md-4"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<input class="form-control" value = "<?php echo $x['dob'] ;?>" type="text" name="dob" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">RACE*</div>
					<div class="col-md-4"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
					    <input class="form-control" value = "<?php echo $x['race'] ;?>" type="text" name="race" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">GENDER*</div>
					<div class="col-md-4"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<input class="form-control" value = "<?php echo $x['gender'] ;?>" type="text" name="gender" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">HEIGHT*</div>
					<div class="col-md-4"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
					<input class="form-control" value = "<?php echo $x['height'];?>" type="text" name="height" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">WEIGHT*</div>
					<div class="col-md-4"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
						<input class="form-control" value = "<?php echo $x['weight'] ;?>" type="text" name="weight" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">EYE COLOR</div>
					<div class="col-md-4"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
					<input class="form-control" value = "<?php echo $x['eye_color'];?>" type="text" name="eye_color" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">HAIR COLOR</div>
					<div class="col-md-4"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
					<input class="form-control" value = "<?php echo $x['hair_color'] ;?>" type="text" name="hair_color" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">SCARS</div>
					<div class="col-md-4"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
					<input class="form-control" value = "<?php echo $s['scars'];?>" type="text" name="scars" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">MARKS</div>
					<div class="col-md-4"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
					<input class="form-control" value = "<?php echo $s['marks'];?>" type="text" name="marks" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">TATTOOS</div>
					<div class="col-md-4"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
					<input class="form-control" value = "<?php echo $s['tattoos'];?>" type="text" name="tattoos" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">MAJOR OFFENSE*</div>
					<div class="col-md-4"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
					<input class="form-control" value = "<?php echo $x['major_offense'];?>" type="text" name="major_offense" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">MOST RECENT INSTITUTE*</div>
					<div class="col-md-4"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
					<input class="form-control" value = "<?php echo $x['most_recent_institute'];?>" type="text" name="most_recent_institute" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">MAX POSSIBLE RELEASE DATE*</div>
					<div class="col-md-4"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
					<input class="form-control" type="text" value = "<?php echo $x['max_possible_release_date'];?>" name="max_possible_release_date" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">ACTUAL RELEASE DATE</div>
					<div class="col-md-4"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
					<input class="form-control" value = "<?php echo $x['actual_release_date'] ;?>" type="text" name="actual_release_date" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">CURRENT STATUS</div>
					<div class="col-md-4"></div> 
				</div><!--row-->
				<div class = "row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
					<input class="form-control" value = "<?php echo $x['current_status'];?>" type="text" name="current_status" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">KNOWN ALIASES</div>
					<div class="col-md-4"></div> 
				</div><!--row-->
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-10">
					<input class="form-control" value = "<?php echo $x['known_aliases'];?>" type="text" name="known_aliases" required/>
					</div>
					<div class="col-md-1"></div>
				</div><!--row-->
				
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-3">ADDITIONAL FIELDS</div>
					<div class="col-md-4"></div> 
				</div><!--row-->
				<div id="add_dv" class="row" style="clear:left;margin-top:1em;">
					 <div style="margin-top:2em;">&nbsp;</div>
					<div class="col-md-1">Name</div>
					<div class="col-md-4"><input id="addnam-99" type="text" name="add_name" class="form-control"/></div>
					<div class="col-md-1">Value</div>
					<div class="col-md-4"><input id="addval-99" type="text" name="add_value" class="form-control"/></div>
					<div class="col-md-2"><input id="hidbtn-99" type="button" onclick = "add_field(this,99)" class="btn btn-default" value="Add"></div>
					<div class="col-md-1"></div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-12">&nbsp;</div>
				</div><!--row-->
				<div class="row">
					<div class="col-md-12">
					<input id="pid" type="hidden" name="pid" value="<?php echo $pid; if(empty($pid)){echo $del_pid;}?>"/>
					</div>
				</div><!--row-->
				
				<div class="row">
					<div class="col-md-5"></div>
						<div class="col-md-2"><input type="submit" name="submit" class="btn btn-default" value="Save"/></div>
					<div class="col-md-5"></div>
				</div><!--row-->
			</form>
		</div><!--container-->
		<script>
			var n=0;
			function add_field(btn,id){
				n++;
				jQuery(btn).hide();
				var adddv = ' <div style="margin-top:2em;">&nbsp;</div>'+'<div style="clear:left" class="col-md-1">Name</div>'+
				'<div class="col-md-4"><input id="addnam-'+n+'" type="text" name="add_name" required class="form-control"/></div>'+
				'<div class="col-md-1">Value</div>'+
				'<div class="col-md-4"><input id="addval-'+n+'" type="text" name="add_value" required class="form-control"/></div>'+
				'<div class="col-md-2"><input id="hidbtn" type="button" onclick = "add_field(this,n)" class="btn btn-default" value="Add"></div>'+
				'<div class="col-md-1"></div>';
				jQuery(adddv).appendTo(this.add_dv);
				var addname = jQuery("#addnam-"+id).val();		
				var addvalue = jQuery("#addval-"+id).val();		
				var post_id = jQuery("#pid").val();		
				var data = {
					'action': 'addtional_info',
					'addname': addname,
					'addvalue': addvalue,
					'post_id':post_id,
				};
				var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
					jQuery.post(ajaxurl, data, function(response) {

					var json = response,
					obj = JSON.parse(json);	
				});	
			}
			//FUNCTION FOR FIELD VALIDATOR
			function check_fields(){
				if((addname == null || addname == '') || (addvalue == null || addvalue == '' )){
					alert('Please Fill The Respective Fields');
					return false;
				}else{
					return true;
				}
			}
		</script>
		<?
		}
		//CUSTOM POST TYPE MOST TYPE
 		function aspk_custom_post_type_fun(){
			$labels = array(
				'name'               => __('Most Wanted'),
				'singular_name'      => __('Most Wanted'),
				'menu_name'          => __( 'Most Wanted'),
				'name_admin_bar'     => __('Most Wanted'),
				'add_new'            => __( 'Add New'),
				'add_new_item'       => __( 'Add New Most Wanted'),
				'new_item'           => __( 'New Most Wanted'),
				'edit_item'          => __('Edit Most Wanted'),
				'view_item'          => __( 'View Most Wanted'),
				'all_items'          =>  __('All Most Wanted'),
				'search_items'       => __( 'Search Most Wanted'),
				'parent_item_colon'  => __( 'Parent Most Wanted:'),
				'not_found'          =>  __('No Most Wanted found.'),
				'not_found_in_trash' =>  __('No Most Wanted found in Trash.'),
			);
			$args = array(
				'labels'             => $labels,
				'public'             => false,
				'publicly_queryable' => false,
				'show_ui'            => false,
				'show_in_menu'       => false,
				'menu_icon'          => 'dashicons-products',
				'query_var'          => true,
				'rewrite'            => array('slug' => 'most_wanted'),
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				//'menu_position'      => 5,
				'supports'           => array('title', 'editor', 'thumbnail')
			);
			register_post_type('most_wanted',$args);
		}//END CUSTOM POST TYPE
	} //class ends 
}//class exists ends
new Agile_Most_Wanted();
