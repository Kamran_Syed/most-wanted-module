<?php
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

/**
 * PHP Server-Side Example for Fine Uploader (traditional endpoint handler).
 * Maintained by Widen Enterprises.
 *
 * This example:
 *  - handles chunked and non-chunked requests
 *  - assumes all upload requests are multipart encoded
 *
 *
 * Follow these steps to get up and running with Fine Uploader in a PHP environment:
 *
 * 1. Setup your client-side code, as documented on http://docs.fineuploader.com.
 *
 * 2. Copy this file and handler.php to your server.
 *
 * 3. Ensure your php.ini file contains appropriate values for
 *    max_input_time, upload_max_filesize and post_max_size.
 *
 * 4. Ensure your "chunks" and "files" folders exist and are writable.
 *    "chunks" is only needed if you have enabled the chunking feature client-side.
 */

// Include the upload handler class
require_once "upload_handler.php";


$uploader = new UploadHandler();

// Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
$uploader->allowedExtensions = array(); // all files types allowed by default

// Specify max file size in bytes.
$uploader->sizeLimit = 10 * 1024 * 1024; // default is 10 MiB

// Specify the input name set in the javascript.
//$result = $uploader->handleUpload(__DIR__."/tmp");
$updir = wp_upload_dir();
$path = $updir['path'];

$uploader->inputName = "qqfile"; // matches Fine Uploader's default inputName value by default

// If you want to use the chunking/resume feature, specify the folder to temporarily save parts.
$uploader->chunksFolder = "chunks";

$method = $_SERVER["REQUEST_METHOD"];



if ($method == "POST") {

    header("Content-Type: text/plain");

    // Call handleUpload() with the name of the folder, relative to PHP's getcwd()
    //$result = $uploader->handleUpload(__DIR__."/tmp");
	if(isset($_GET['act'])){
		if($_GET['act']=='del'){
			$uploader->delete_primary_img();	
		}
	} 
	if(isset($_GET['multi'])){
		if($_GET['multi']=='del'){
			$uploader->delete_multi_images();	
		}
	} 
	
	if(isset($_GET['prim'])){
		if($_GET['prim']=='yes'){
		$uploader->handleUpload_primary_img($path);	
		
		}
	} 
	$result = $uploader->handleUpload($path);
	
    // To return a name used for uploaded file you can use the following line.
    $result["uploadName"] = $uploader->getUploadName();
	unset($result['path']);
	unset($result['url']);
	unset($result['id']);
    echo json_encode($result);
	
}else {
    header("HTTP/1.0 405 Method Not Allowed");
}

?>